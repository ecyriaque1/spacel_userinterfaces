// 1. Module imports
import { useState } from "react";
import { CSSTransition } from "react-transition-group";

// 2. Components imports
import SignupForm from "../components/userFormComponents/SignupForm";
import SigninForm from "../components/userFormComponents/SigninForm";

// 3. Style imports
import "./css/Sign.css";
import "react-toastify/dist/ReactToastify.css";
import ProtectedRoute from "../components/ProtectedRoute";

const FORMS = {
  NONE: "NONE",
  SIGNIN: "SIGNIN",
  SIGNUP: "SIGNUP",
};

const AuthentificationForm = (toast) => {
  const [activeForm, setActiveForm] = useState(FORMS.SIGNIN);
  const showSigninForm = () => setActiveForm(FORMS.SIGNIN);
  const showSignupForm = () => setActiveForm(FORMS.SIGNUP);
  const closeForms = () => setActiveForm(FORMS.NONE);

  return (
    <div className="App">
      <ProtectedRoute redirectLink="/" requireAuth={false} />
      <div className="sign-container">
        <CSSTransition
          in={activeForm === FORMS.SIGNIN}
          timeout={300}
          classNames="fade"
          unmountOnExit
        >
          <SigninForm onSignupClick={showSignupForm} />
        </CSSTransition>
        <CSSTransition
          in={activeForm === FORMS.SIGNUP}
          timeout={300}
          classNames="fade"
          unmountOnExit
        >
          <SignupForm
            toast={toast}
            onSigninClick={showSigninForm}
            onClose={closeForms}
          />
        </CSSTransition>
      </div>
    </div>
  );
};

export default AuthentificationForm;
