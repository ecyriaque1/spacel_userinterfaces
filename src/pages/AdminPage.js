import React, { useState } from "react";
import ManageUsers from "../components/adminComponents/AdminManageUser/ManageUser";
import AdminSidebar from "../components/adminComponents/AdminSideBar";
import CreateUser from "../components/adminComponents/AdminManageUser/CreateUser";
import EditUser from "../components/adminComponents/AdminManageUser/EditUser";
import UserDeleteModal from "../components/adminComponents/AdminManageUser/UserDeleteModal";
import "./css/AdminPage.css";

import ProtectedRoute from "../components/ProtectedRoute";

const AdminPage = () => {
  const [activeComponent, setActiveComponent] = useState("Dashboard");
  const [editUser, setEditUser] = useState(null); // Add state to store the user being edited
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);

  const handleDelete = (userId) => {
    alert("Suppression de l'utilisateur :" + userId);
  };

  // Affichage du modal de suppression d'utilisateur
  const showDeleteConfirmation = (user) => {
    setSelectedUser(user);
    setShowDeleteModal(true);
  };

  const hideDeleteConfirmation = () => {
    setShowDeleteModal(false);
  };

  const handleEditUser = (user) => {
    setEditUser(user); // Set the user being edited
    setActiveComponent("EditUser"); // Switch to the EditUser component
  };

  const handleComponentChange = (component) => {
    setActiveComponent(component);
  };

  return (
    <div>
      <ProtectedRoute redirectLink="/AuthentificationForm" requireAuth={true} />
      <div className="admin-page-container">
        <div className="sidebar-container">
          <AdminSidebar
            activeComponent={activeComponent}
            handleComponentChange={handleComponentChange}
            showDeleteConfirmation={showDeleteConfirmation}
          />
        </div>
        <div className="main-content">
          {activeComponent === "Dashboard" && <DashboardComponent />}
          {activeComponent === "ManageUsers" && (
            <ManageUsers
              handleEditUser={handleEditUser}
              showDeleteConfirmation={showDeleteConfirmation}
            />
          )}
          {activeComponent === "CreateUser" && <CreateUser />}
          {activeComponent === "EditUser" && (
            <EditUser
              user={editUser}
              showDeleteConfirmation={showDeleteConfirmation}
            />
          )}{" "}
          {/* Pass the user object and showDeleteConfirmation function as props */}
        </div>
      </div>
      {(activeComponent === "ManageUsers" ||
        activeComponent === "EditUser") && (
        <UserDeleteModal
          user={selectedUser}
          show={showDeleteModal}
          onHide={hideDeleteConfirmation}
          onDelete={handleDelete}
        />
      )}
    </div>
  );
};

const DashboardComponent = () => <div>Dashboard Component</div>;

export default AdminPage;
