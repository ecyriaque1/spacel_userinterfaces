import React, { useContext, useState } from "react";
import { Container, Card, Modal, ListGroup, Button } from "react-bootstrap";
import Graph from "react-graph-vis";
import ToastContext from "../contexts/ToastContext";
import { API_BASE_URL } from "../config/config";

const AfficherFormation = () => {
  const [showModal, setShowModal] = useState(false);
  const [modalContent, setModalContent] = useState({});
  const [validatedLOs, setValidatedLOs] = useState([]);
  const toast = useContext(ToastContext);

  const [listeLosFormation] = useState([
    { idcurrlevello: "LO0" },
    { idcurrlevello: "LO1" },
    { idcurrlevello: "LO10" },
    { idcurrlevello: "LO11" },
    { idcurrlevello: "LO2" },
    { idcurrlevello: "LO3" },
    { idcurrlevello: "LO4" },
    { idcurrlevello: "LO5" },
    { idcurrlevello: "LO6" },
    { idcurrlevello: "LO7" },
    { idcurrlevello: "LO8" },
    { idcurrlevello: "LO9" },
  ]);

  const [listeLosNiveau] = useState([
    {
      idPreviouslevel: "LO0",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO0",
      idcurrlevello: "LO2",
      idnextlo: "LO3",
    },
    {
      idPreviouslevel: "LO0",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO0",
      idcurrlevello: "LO2",
      idnextlo: "LO4",
    },
    {
      idPreviouslevel: "LO1",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO1",
      idcurrlevello: "LO2",
      idnextlo: "LO3",
    },
    {
      idPreviouslevel: "LO1",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO1",
      idcurrlevello: "LO2",
      idnextlo: "LO4",
    },
    {
      idPreviouslevel: "LO2",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO2",
      idcurrlevello: "LO3",
      idnextlo: "LO5",
    },
    {
      idPreviouslevel: "LO2",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO2",
      idcurrlevello: "LO3",
      idnextlo: "LO6",
    },
    {
      idPreviouslevel: "LO2",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO2",
      idcurrlevello: "LO4",
      idnextlo: "LO5",
    },
    {
      idPreviouslevel: "LO2",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO2",
      idcurrlevello: "LO4",
      idnextlo: "LO6",
    },
    {
      idPreviouslevel: "LO3",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO3",
      idcurrlevello: "LO5",
      idnextlo: "LO7",
    },
    {
      idPreviouslevel: "LO3",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO3",
      idcurrlevello: "LO6",
      idnextlo: "LO7",
    },
    {
      idPreviouslevel: "LO4",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO4",
      idcurrlevello: "LO5",
      idnextlo: "LO7",
    },
    {
      idPreviouslevel: "LO4",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO4",
      idcurrlevello: "LO6",
      idnextlo: "LO7",
    },
    {
      idPreviouslevel: "LO5",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO5",
      idcurrlevello: "LO7",
      idnextlo: "LO10",
    },
    {
      idPreviouslevel: "LO5",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO5",
      idcurrlevello: "LO7",
      idnextlo: "LO11",
    },
    {
      idPreviouslevel: "LO5",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO5",
      idcurrlevello: "LO7",
      idnextlo: "LO8",
    },
    {
      idPreviouslevel: "LO5",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO5",
      idcurrlevello: "LO7",
      idnextlo: "LO9",
    },
    {
      idPreviouslevel: "LO6",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO6",
      idcurrlevello: "LO7",
      idnextlo: "LO10",
    },
    {
      idPreviouslevel: "LO6",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO6",
      idcurrlevello: "LO7",
      idnextlo: "LO11",
    },
    {
      idPreviouslevel: "LO6",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO6",
      idcurrlevello: "LO7",
      idnextlo: "LO8",
    },
    {
      idPreviouslevel: "LO6",
      Previouslevel:
        "http://linc.iut.univ-paris8.fr/learningCafe/Training.owlLO6",
      idcurrlevello: "LO7",
      idnextlo: "LO9",
    },
  ]);
  const [listeLosSansPrerequis] = useState([
    {
      idcurrlevello: "LO0",
      idnextlo: "LO2",
    },
    {
      idcurrlevello: "LO1",
      idnextlo: "LO2",
    },
  ]);

  // useEffect(() => {
  //   $.ajax({
  //     url: `${API_BASE_URL}/ListofCurLosWithPresq`,
  //     method: "POST",
  //     dataType: "json",
  //     data: { tr: formData.idTraining },
  //     success: function (response) {
  //       console.log("liste de tous les Los lié a une formation" + response);
  //     },
  //     error: function (error) {
  //       console.log(
  //         "liste de tous les Los lié a une formation error" +
  //           JSON.stringify(error)
  //       );
  //     },
  //   });
  //   $.ajax({
  //     url: `${API_BASE_URL}/LosNoPrereq`,
  //     method: "POST",
  //     dataType: "json",
  //     data: { tr: formData.idTraining },
  //     success: function (response) {
  //       console.log("List des LO sans prérequis" + response);
  //     },
  //     error: function (error) {
  //       console.log("List des LO sans prérequis error" + JSON.stringify(error));
  //     },
  //   });
  //   $.ajax({
  //     url: `${API_BASE_URL}/listlo`,
  //     method: "GET",
  //     dataType: "json",
  //     headers: {
  //       "content-type": "application/json",
  //     },
  //     data: { tr: formData.idTraining },
  //     success: function (response) {
  //       console.log("List des LO par niveau" + response);
  //     },
  //     error: function (error) {
  //       console.log("List des LO par niveau error" + JSON.stringify(error));
  //     },
  //   });
  //   $.ajax({
  //     url: `${API_BASE_URL}/listofAllPedagogicalResources`,
  //     method: "GET",
  //     dataType: "json",
  //     data: { Train: formData.idTraining },
  //     headers: {
  //       "content-type": "application/json",
  //     },
  //     success: function (response) {
  //       console.log("liste des resources :" + response);
  //     },
  //     error: function (error) {
  //       console.log("liste des resources error :" + error);
  //     },
  //   });
  // }, []);

  const addFakeResources = (los) => {
    return los.map((lo) => ({
      ...lo,
      resources: ["Resource 1", "Resource 2", "Resource 3"], // Ajout des fausses ressources
    }));
  };

  let los = addFakeResources(listeLosSansPrerequis).map((item) => ({
    id: item.idcurrlevello,
    titre: item.idcurrlevello.replace("LO", "LO "),
    resources: item.resources,
    color: validatedLOs.includes(item.idcurrlevello) ? "#00ff00" : "#ff0000", // Changement de couleur en fonction de la validation
  }));

  const loToPrereqs = {};
  listeLosNiveau.forEach((item) => {
    const lo = item.idcurrlevello;
    const prereq = item.idPreviouslevel;

    if (!loToPrereqs[lo]) {
      loToPrereqs[lo] = [];
    }

    if (!loToPrereqs[lo].includes(prereq)) {
      loToPrereqs[lo].push(prereq);
    }
  });

  for (let lo in loToPrereqs) {
    loToPrereqs[lo].sort();
  }

  const levels = {};
  for (let lo in loToPrereqs) {
    const prereqs = loToPrereqs[lo];
    const key = prereqs.join(", ");

    if (!levels[key]) {
      levels[key] = {
        los: [],
        prerequis: prereqs.map((id) => id.replace("LO", "LO ")),
      };
    }

    levels[key].los = addFakeResources(levels[key].los);

    levels[key].los.push({
      id: lo,
      titre: lo.replace("LO", "LO "),
      resources: [], // Ajout des fausses ressources
      color: validatedLOs.includes(lo) ? "#00ff00" : "#ff0000", // Changement de couleur en fonction de la validation
    });
  }

  const niveaux = Object.values(levels);

  const loInNiveaux = new Set();

  for (const niveau of listeLosNiveau) {
    loInNiveaux.add(niveau.idcurrlevello);
  }

  for (const lo of listeLosSansPrerequis) {
    loInNiveaux.add(lo.idcurrlevello);
  }

  const dernierNiveauLos = listeLosFormation.filter(
    (lo) => !loInNiveaux.has(lo.idcurrlevello)
  );

  const dernierNiveau = {
    los: addFakeResources(dernierNiveauLos).map((lo) => ({
      id: lo.idcurrlevello,
      titre: lo.idcurrlevello,
      resources: lo.resources,
      color: validatedLOs.includes(lo.idcurrlevello) ? "#00ff00" : "#ff0000", // Changement de couleur en fonction de la validation
    })),
    prerequis: [],
  };

  niveaux.push(dernierNiveau);

  const parcours = {
    id: 1,
    titre: "Parcours Exemple",
    niveaux: [
      {
        los,
      },
    ],
  };
  parcours.niveaux = parcours.niveaux.concat(niveaux);

  const cheminRouge = [];

  const options = {
    layout: {
      hierarchical: {
        direction: "UD",
        sortMethod: "directed",
      },
    },
    edges: {
      color: "#000000",
    },
    interaction: {
      zoomView: false,
      dragView: false,
      dragNodes: false,
    },
    height: "500px",
  };

  const events = {
    click: function (event) {
      const { nodes } = event;
      if (nodes.length === 1) {
        const nodeID = nodes[0];
        const node = graph.nodes.find((node) => node.id === nodeID);

        if (node) {
          const lo = parcours.niveaux
            .map((niveau) => niveau.los)
            .flat()
            .find((lo) => lo.titre === node.label);

          const niveauIndex = parcours.niveaux.findIndex((niveau) =>
            niveau.los.includes(lo)
          );

          const prerequisComplete =
            niveauIndex === 0 ||
            parcours.niveaux[niveauIndex - 1].los.some((lo) =>
              validatedLOs.includes(lo.id)
            );

          if (!prerequisComplete) {
            toast.error(
              "Au moins l'un des prérequis pour ce LO doit être validé !"
            );
            return;
          }

          if (lo) {
            setModalContent(lo);
            setShowModal(true);
          }
        }
      }
    },
  };

  const validateLO = (lo) => {
    setValidatedLOs((prevLOs) => [...prevLOs, lo.id]);
    setShowModal(false);
    toast.success(
      "Vous avez bien validé le LO. Vous pouvez passer au suivant."
    );
  };

  const graph = {
    nodes: [],
    edges: [],
  };

  let nodeId = 0;
  let edgeId = 0;

  parcours.niveaux.forEach((niveau, niveauIndex) => {
    niveau.los.forEach((lo) => {
      const nodeID = `node-${parcours.id}-${nodeId++}`;
      graph.nodes.push({
        id: nodeID,
        label: lo.titre,
        title: lo.titre,
        color: lo.color,
      });

      if (niveauIndex > 0) {
        const prerequis = parcours.niveaux[niveauIndex - 1].los;

        prerequis.forEach((prerequisLo) => {
          const prerequisNodeId = graph.nodes.find(
            (node) => node.label === prerequisLo.titre
          ).id;

          const isRedPath =
            cheminRouge.includes(prerequisLo.titre) &&
            cheminRouge.includes(lo.titre);

          graph.edges.push({
            id: `edge-${parcours.id}-${edgeId++}`,
            from: prerequisNodeId,
            to: nodeID,
            color: isRedPath ? "red" : "#000000",
          });
        });
      }
    });
  });

  return (
    <Container>
      <h1 className="text-center mb-4">{parcours.titre}</h1>
      <Card>
        <Card.Body>
          <Graph graph={graph} options={options} events={events} />
        </Card.Body>
      </Card>{" "}
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>{modalContent.titre} Resources</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ListGroup variant="flush">
            {modalContent.resources &&
              modalContent.resources.map((resource, index) => (
                <ListGroup.Item key={index}>{resource}</ListGroup.Item>
              ))}
          </ListGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => validateLO(modalContent)}>
            Valider le LO
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
};

export default AfficherFormation;
