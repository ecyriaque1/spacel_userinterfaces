// Importing necessary dependencies and components
import React, { useState, useEffect } from "react";
import { Form, Button, Row, Col, Alert } from "react-bootstrap";
import "./css/TrainingPage.css";
import $ from "jquery";
import Select from "react-select";
import ProtectedRoute from "../components/ProtectedRoute";
import { useNavigate } from "react-router-dom";
import { API_BASE_URL } from "../config/config.js";
function TrainingPage() {
  // State for form data and initial values
  var userData = JSON.parse(sessionStorage.getItem("userData"));

  const [formData, setFormData] = useState({
    idTraining: "DEV",
    userName: userData.name,
    idUser: userData.idUser,
    levelExp: "",
    DureePref: "",
    preferredlangOfResource: "",
    preferredResourceFormat: "",
    MotivationOfLearning: 0,
  });

  // State for training list and initial value
  const [trainingList, setTrainingList] = useState([]);

  // State to track if motivation field has changed
  const [motivationChange, setMotivationChange] = useState(false);

  //state for Error

  const [showTrainingError, setShowTrainingError] = useState(false);
  const [showMotivationError, setShowMotivationErrorr] = useState(false);
  // Function to handle form field changes
  const handleChange = (event) => {
    const { name, value } = event.target;

    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));

    if (name === "MotivationOfLearning") setMotivationChange(true);
  };

  // Function to validate the DureePref field
  const isDurationValid = () => {
    const duration = formData.DureePref;
    // Check if the duration is a positive integer and does not contain "." or ","
    if (
      Number.isInteger(+duration) &&
      +duration >= 0 &&
      !/\./.test(duration) &&
      !/,/.test(duration)
    ) {
      return true;
    } else {
      return false;
    }
  };

  const isTrainingValid = () => {
    if (formData.idTraining === "") {
      setShowTrainingError(true);
      return false;
    } else {
      setShowTrainingError(false);
      return true;
    }
  };

  // Function to validate the motivation field
  const isMotivationValid = () => {
    if (motivationChange) {
      setShowMotivationErrorr(false);
      return true;
    } else {
      setShowMotivationErrorr(true);
      return false;
    }
  };

  const navigate = useNavigate();
  // Function to handle form submission
  const handleSubmit = (event) => {
    event.preventDefault();

    const isMValid = isMotivationValid();
    const isTValid = isTrainingValid();
    const isDValid = isDurationValid();
    for (var i = 0; i < sessionStorage.length; i++) {
      var key = sessionStorage.key(i);
      var value = sessionStorage.getItem(key);
      console.log("Clé : " + key + ", Valeur : " + value);
    }

    // navigate({
    //   pathname: "/LearningGraphDraft",
    //   state: { data: formData },
    // });

    if (isMValid && isTValid && isDValid) {
      console.log(JSON.stringify(formData));
      navigate("/afficherFormation", { state: { data: formData } });

      //   $.ajax({
      //     url: `${API_BASE_URL}/newtrainingsession`,
      //     method: "POST",
      //     data: JSON.stringify(formData),
      //     contentType: "application/json; charset=utf-8",
      //     dataType: "json",
      //     crossDomain: true,
      //     async: true,
      //     success: function (response) {
      //       console.log(response);
      //       navigate("/afficherFormation", { state: { data: formData } });
      //     },
      //     error: function (response, error) {
      //       console.log("error :" + response);
      //       // console.error("Erreur lors de l’envoi des données:", error);
      //     },
      //   });
    }
  };

  // useEffect to fetch the training list data
  useEffect(() => {
    const ListTraining = {
      async: true,
      crossDomain: true,
      url: `${API_BASE_URL}/ListTraining`,
      method: "GET",
      headers: {
        "content-type": "application/json",
      },
    };
    $.ajax(ListTraining).done(function (ListTrainingData) {
      const formattedList = JSON.parse(ListTrainingData).map(
        (training, index) => ({
          id: index + 1,
          name: training.idtraining,
        })
      );
      setTrainingList(formattedList);
      console.log("FORM:" + JSON.stringify(formattedList));
    });
  }, []);

  return (
    <div className="training-form-container">
      <ProtectedRoute redirectLink="/AuthentificationForm" requireAuth={true} />
      <h3> Formations :</h3>
      <Form className="training-form" onSubmit={handleSubmit}>
        <Row className="mb-3">
          <Form.Label column sm={2}>
            Formation :
          </Form.Label>
          <Col sm={10}>
            <Form.Group controlId="formTraining">
              <Select
                options={trainingList.map((training) => ({
                  value: training.name,
                  label: training.name,
                }))}
                name="training"
                onChange={(selectedOption) => {
                  setFormData({
                    ...formData,
                    idTraining: selectedOption.value,
                  });
                  isTrainingValid();
                }}
              />
              {showTrainingError && (
                <Alert variant="danger" type="invalid">
                  Veuillez sélectionner une formation
                </Alert>
              )}
            </Form.Group>
          </Col>
        </Row>

        <Row className="mb-3">
          <Form.Label column sm={2}>
            Langue :
          </Form.Label>
          <Col sm={10}>
            <Form.Group controlId="formpreferredlangOfResource">
              <Form.Control
                as="select"
                name="preferredlangOfResource"
                value={formData.preferredlangOfResource}
                onChange={handleChange}
                required
              >
                <option value="">Choisir...</option>
                <option value="en">Anglais</option>
                <option value="fr">Français</option>
                <option value="ar">Arabe</option>
                <option value="es">Espagnol</option>
                <option value="de">Allemand</option>
                <option value="it">Italien</option>
                <option value="ru">Russe</option>
                <option value="pt">Portugais</option>
                <option value="zh">Chinois</option>
                <option value="ja">Japonais</option>
                <option value="ko">Coréen</option>
                <option value="hi">Hindi</option>
              </Form.Control>
            </Form.Group>
          </Col>
        </Row>
        <Row className="mb-3">
          <Form.Label column sm={2}>
            Durée(minute):
          </Form.Label>
          <Col sm={10}>
            <Form.Group controlId="formDureePref">
              <Form.Control
                type="number"
                min="0"
                step="1"
                name="DureePref"
                value={formData.DureePref}
                placeholder="Entrez une durée en minute"
                onChange={handleChange}
                required
                isInvalid={!isDurationValid()}
              />
              <Form.Control.Feedback type="invalid">
                Veuillez entrer un chiffre entier positif.
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>

        <Row className="mb-3">
          <Form.Label column sm={2}>
            Niveau :
          </Form.Label>
          <Col sm={10}>
            <Form.Group controlId="formlevelExp">
              <Form.Control
                as="select"
                name="levelExp"
                value={formData.levelExp}
                onChange={handleChange}
                required
              >
                <option value="">Choisir...</option>
                <option value="1">Débutant</option>
                <option value="2">Intermédiaire</option>
                <option value="3">Expert</option>
              </Form.Control>
            </Form.Group>
          </Col>
        </Row>
        <Row className="mb-3">
          <Form.Label column sm={2}>
            Format :
          </Form.Label>
          <Col sm={10}>
            <Form.Group controlId="formFormat">
              <Form.Control
                as="select"
                name="preferredResourceFormat"
                value={formData.preferredResourceFormat}
                onChange={handleChange}
                required
              >
                <option value="">Choisir...</option>
                <option value="text">Texte</option>
                <option value="video">Vidéo</option>
                <option value="audio">Audio</option>
              </Form.Control>
            </Form.Group>
          </Col>
        </Row>

        <Row className="mb-3">
          <Form.Label column sm={2}>
            Motivation :
          </Form.Label>
          <Col sm={10}>
            <Form.Group controlId="formMotivation">
              <Form.Label>{formData.MotivationOfLearning}%</Form.Label>
              <Form.Control
                type="range"
                min="0"
                max="100"
                step="1"
                name="MotivationOfLearning"
                value={formData.MotivationOfLearning}
                onChange={handleChange}
              />
              {showMotivationError && (
                <Alert variant="danger" type="invalid">
                  veuillez indiquer votre motivation en séléctionnant un
                  pourcentage
                </Alert>
              )}
            </Form.Group>
          </Col>
        </Row>

        <Row>
          <Col
            sm={{ span: 10, offset: 2 }}
            className="d-flex align-items-center"
          >
            <Button variant="primary" type="submit" className="mr-2">
              Envoyer
            </Button>
          </Col>
        </Row>
      </Form>
    </div>
  );
}
export default TrainingPage;
