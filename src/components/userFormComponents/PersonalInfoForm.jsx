import React, { useState } from "react";

import { Form, Button, Row, Col } from "react-bootstrap";

const PersonalInfoForm = ({
  formData,
  setFormData,
  nextStep,
  onSigninClick,
}) => {
  const [EmailError, setEmailError] = useState("");
  const [PasswordError, setPasswordError] = useState("");
  const [confirmPasswordError, setconfirmPasswordError] = useState("");
  const [lastNameError, setLastNameError] = useState("");
  const [firstNameError, setFirstNameError] = useState("");
  const [genderError, setGenderError] = useState("");
  const [birthdateError, setBirthdateError] = useState("");

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));

    switch (name) {
      case "email":
        isEmailValid(value);
        break;

      case "password":
        isPasswordValid(value);
        break;

      case "confirmPassword":
        isConfirmPassword(formData.password, value);
        break;

      case "lastName":
        isLastNameValid(value);
        break;
      case "birthdate":
        isBirthdateValid(value);
        break;

      case "firstName":
        isFirstNameValid(value);
        break;
      default:
        break;
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    const emailIsValid = isEmailValid(formData.email);
    const passwordIsValid = isPasswordValid(formData.password);
    const confirmPasswordIsValid = isConfirmPassword(
      formData.password,
      formData.confirmPassword
    );
    const lastNameIsValid = isLastNameValid(formData.lastName);
    const firstNameIsValid = isFirstNameValid(formData.firstName);
    const genderNameIsValid = isGenderValid(formData.gender);
    const dateOfBirthIsValid = isBirthdateValid(formData.dateOfBirth);

    if (
      emailIsValid &&
      passwordIsValid &&
      confirmPasswordIsValid &&
      lastNameIsValid &&
      firstNameIsValid &&
      genderNameIsValid &&
      dateOfBirthIsValid
    ) {
      nextStep();
    }
  };

  const isEmailValid = (email) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (email === "" || !emailRegex.test(email)) {
      setEmailError("Veuillez saisir une adresse e-mail valide.");
      return false;
    } else {
      setEmailError("");
      return true;
    }
  };

  const isPasswordValid = (password) => {
    const passwordRegex =
      /^(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_+])[A-Za-z\d!@#$%^&*()_+]{8,}$/;
    if (password === "" || !passwordRegex.test(password)) {
      setPasswordError(
        "Le mot de passe doit contenir au moins 8 caractères, dont une majuscule, un chiffre et un caractère spécial parmi les suivants : !@#$%^&*()_+."
      );
      return false;
    } else {
      setPasswordError("");
      return true;
    }
  };

  const isGenderValid = (gender) => {
    if (gender === "") {
      setGenderError("Veuillez faire un choix .");
      return false;
    } else {
      setGenderError("");
      return true;
    }
  };

  const isConfirmPassword = (password, confirmPassword) => {
    if (confirmPassword !== password && confirmPassword !== "") {
      setconfirmPasswordError("Les mots de passe ne correspondent pas.");
      return false;
    } else {
      setconfirmPasswordError("");
      return true;
    }
  };

  const isLastNameValid = (lastName) => {
    const lastNameRegex = /^[a-zA-ZÀ-ÿ]+([-'\s][a-zA-ZÀ-ÿ]+)*$/;
    if (lastName === "" || !lastNameRegex.test(lastName.trim())) {
      setLastNameError("Veuillez saisir un nom valide.");
      return false;
    } else {
      setLastNameError("");
      return true;
    }
  };

  const isFirstNameValid = (firstName) => {
    const firstNameRegex = /^[a-zA-ZÀ-ÿ]+([-'\s][a-zA-ZÀ-ÿ]+)*$/;
    if (firstName === "" || !firstNameRegex.test(firstName.trim())) {
      setFirstNameError("Veuillez saisir un prénom valide.");
      return false;
    } else {
      setFirstNameError("");
      return true;
    }
  };

  const isBirthdateValid = (birthdate) => {
    // Add your birthdate validation logic here
    // Example validation: Check if birthdate is not empty
    if (birthdate === "") {
      setBirthdateError("Veuillez saisir une date de naissance valide.");
      return false;
    }

    const selectedDate = new Date(birthdate);
    const currentDate = new Date();

    if (selectedDate.getFullYear() > currentDate.getFullYear()) {
      setBirthdateError(
        "L'année de naissance ne peut pas être supérieure à l'année actuelle."
      );
      return false;
    }

    setBirthdateError("");
    return true;
  };

  return (
    <div>
      <h3>Informations personnelles :</h3>
      <Form className="signup-form" onSubmit={handleSubmit}>
        <Row className="mb-3">
          <Form.Label column sm={2}>
            Nom
          </Form.Label>
          <Col sm={5}>
            {/* Nom */}
            <Form.Group controlId="formLastName">
              <Form.Control
                placeholder="Nom"
                type="text"
                name="lastName"
                value={formData.lastName}
                onChange={handleChange}
                required
                isInvalid={!!lastNameError}
              />
              <Form.Control.Feedback type="invalid">
                {lastNameError}
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
          <Col sm={5}>
            <Form.Group controlId="formFirstName">
              <Form.Control
                placeholder="Prénom"
                type="text"
                name="firstName"
                value={formData.firstName}
                onChange={handleChange}
                required
                isInvalid={!!firstNameError}
              />
              <Form.Control.Feedback type="invalid">
                {firstNameError}
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>
        <Row className="mb-3">
          <Form.Label column sm={2}>
            Date de naissance
          </Form.Label>
          <Col sm={10}>
            <Form.Group controlId="formdateOfBirth">
              <Form.Control
                type="date"
                name="dateOfBirth"
                value={formData.dateOfBirth}
                onChange={handleChange}
                required
                isInvalid={!!birthdateError}
              />
              <Form.Control.Feedback type="invalid">
                {birthdateError}
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>

        <Row className="mb-3">
          <Form.Label column sm={2}>
            Email
          </Form.Label>
          <Col sm={10}>
            <Form.Group controlId="formEmail">
              <Form.Control
                placeholder="Email"
                type="email"
                name="email"
                value={formData.email}
                onChange={handleChange}
                required
                isInvalid={!!EmailError}
              />
              <Form.Control.Feedback type="invalid">
                {EmailError}
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>
        <Row className="mb-3">
          <Form.Label column sm={2}>
            Genre
          </Form.Label>
          <Col sm={10}>
            <Form.Group controlId="formGender">
              <Form.Control
                as="select"
                name="gender"
                value={formData.gender}
                onChange={handleChange}
                required
                isInvalid={!!genderError}
              >
                <option value="">Choisir...</option>
                <option value="homme">Homme</option>
                <option value="femme">Femme</option>
                <option value="autre">Autre</option>
              </Form.Control>
              <Form.Control.Feedback type="invalid">
                {genderError}
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>
        <Row className="mb-3">
          <Form.Label column sm={2}>
            Mot de passe
          </Form.Label>
          <Col sm={10}>
            <Form.Group controlId="formPassword">
              <Form.Control
                placeholder="Mot de passe"
                type="password"
                name="password"
                value={formData.password}
                onChange={handleChange}
                required
                isInvalid={!!PasswordError}
              />
              <Form.Control.Feedback type="invalid">
                {PasswordError}
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>
        <Row className="mb-3">
          <Form.Label column sm={2}>
            Confirmation
          </Form.Label>
          <Col sm={10}>
            <Form.Group controlId="formConfirmPassword">
              <Form.Control
                placeholder="Confirmer le mot de passe"
                type="password"
                name="confirmPassword"
                value={formData.confirmPassword}
                onChange={handleChange}
                required
                isInvalid={!!confirmPasswordError}
                onKeyPress={(event) => {
                  if (event.key === "Enter") {
                    event.preventDefault(); // Empêche le rechargement de la page
                    handleSubmit(event); // Passe l'événement en argument de la fonction handleSubmit
                  }
                }}
              />
              <Form.Control.Feedback type="invalid">
                {confirmPasswordError}
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>
        <hr />
        <Row className="mb-3">
          <Col sm={{ span: 10, offset: 2 }}>
            <Button variant="primary" onClick={handleSubmit}>
              Continuer
            </Button>
            <span className="ml-2">
              Vous possédez déjà un compte?
              <button
                onClick={onSigninClick}
                style={{
                  backgroundColor: "transparent",
                  border: "none",
                  color: "blue",
                  textDecoration: "underline",
                  cursor: "pointer",
                }}
              >
                Connectez-vous
              </button>
            </span>
          </Col>
        </Row>
      </Form>
    </div>
  );
};

export default PersonalInfoForm;
//getTraining
