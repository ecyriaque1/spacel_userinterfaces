import React, { useState } from "react";
import { Button, ListGroup, Form, Alert } from "react-bootstrap";

function RecapForm({ formData, prevStep, handleSubmit, onSigninClick }) {
  const [acceptConditions, setAcceptConditions] = useState(false);
  const [showError, setShowError] = useState(false);

  const handleCheckboxChange = (event) => {
    setAcceptConditions(event.target.checked);
  };

  const handleFormSubmit = () => {
    if (acceptConditions) {
      handleSubmit();
    } else {
      setShowError(true);
    }
  };
  return (
    <div>
      <h2>Récapitulatif :</h2>
      <ListGroup>
        <ListGroup.Item>
          <strong>Nom :</strong> {formData.lastName}
        </ListGroup.Item>
        <ListGroup.Item>
          <strong>Prénom : </strong> {formData.firstName}
        </ListGroup.Item>
        <ListGroup.Item>
          <strong>Genre :</strong> {formData.gender}
        </ListGroup.Item>
        <ListGroup.Item>
          <strong>Date de naissance :</strong> {formData.dateOfBirth}
        </ListGroup.Item>

        <ListGroup.Item>
          <strong>Email :</strong> {formData.email}
        </ListGroup.Item>

        <ListGroup.Item>
          <strong>Nom d'utilisateur :</strong> {formData.username}
        </ListGroup.Item>
        <ListGroup.Item>
          <strong>Mode d'apprentissage collectif préféré :</strong>{" "}
          {formData.PreferredCollectiveLearningMode}%
        </ListGroup.Item>
        <ListGroup.Item>
          <strong>Mode d'apprentissage individuel préféré :</strong>{" "}
          {formData.PreferredIndividualLearningMode}%
        </ListGroup.Item>
        <ListGroup.Item>
          <strong>Format audio préféré :</strong>{" "}
          {formData.PreferredFormatAudio}%
        </ListGroup.Item>
        <ListGroup.Item>
          <strong>Format texte préféré :</strong>
          {""}
          {formData.PreferredFormatText}%
        </ListGroup.Item>
        <ListGroup.Item>
          <strong>Format Vidéo préféré :</strong>{" "}
          {formData.PreferredFormatVideo}%
        </ListGroup.Item>
      </ListGroup>
      <Form.Group controlId="formBasicCheckbox">
        <Form.Check
          type="checkbox"
          label="J'accepte les conditions d'utilisation"
          checked={acceptConditions}
          onChange={handleCheckboxChange}
        />
      </Form.Group>
      {showError && (
        <Alert variant="danger">
          Veuillez accepter les conditions d'utilisation pour continuer.
        </Alert>
      )}
      <Button variant="secondary" onClick={prevStep}>
        Précédent
      </Button>{" "}
      <Button variant="primary" type="submit" onClick={handleFormSubmit}>
        Soumettre
      </Button>
      <span className="ml-2">
        Vous possédez déjà un compte?
        <button
          style={{
            background: "none",
            border: "none",
            padding: 0,
            textDecoration: "underline",
            cursor: "pointer",
            color: "blue",
          }}
          onClick={onSigninClick}
        >
          Créez un compte
        </button>
      </span>
    </div>
  );
}

export default RecapForm;
