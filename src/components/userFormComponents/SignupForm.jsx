import React, { useContext, useState } from "react";
import PersonalInfoForm from "./PersonalInfoForm";
import PreferenceForm from "./PreferenceForm";
import RecapForm from "./RecapForm";
import "./css/SignupForm.css";
import $ from "jquery";
import ToastContext from "../../contexts/ToastContext";
import { API_BASE_URL } from "../../config/config.js";
const TOTAL_STEPS = 3;
const STEP_TITLES = [
  "Informations personnelles ",
  "Préférences",
  "Récapitulatif",
];

function SignupForm({ onSigninClick, toast }) {
  toast = useContext(ToastContext);
  const [formData, setFormData] = useState({
    idUser: Math.floor(Math.random() * 1000000000),
    firstName: "",
    lastName: "",
    email: "",
    gender: "",
    username: "",
    password: "",
    dateOfBirth: "",
    PreferredCollectiveLearningMode: -1,
    PreferredIndividualLearningMode: -1,
    PreferredFormatAudio: -1,
    PreferredFormatText: -1,
    PreferredFormatVideo: -1,
  });
  const [currentStep, setCurrentStep] = useState(1);
  const [animationClass, setAnimationClass] = useState("");

  const nextStep = () => {
    setAnimationClass("slide-out");
    setTimeout(() => {
      setCurrentStep(currentStep + 1);
      setAnimationClass("");
    }, 500);
  };

  const prevStep = () => {
    setAnimationClass("slide-out");
    setTimeout(() => {
      setCurrentStep(currentStep - 1);
      setAnimationClass("");
    }, 500);
  };

  const handleSubmit = () => {
    console.log(JSON.stringify(formData));

    const trimFormData = { ...formData };
    for (let field in trimFormData) {
      if (typeof trimFormData[field] === "string") {
        trimFormData[field] = trimFormData[field].trim();
      }
    }

    console.log(JSON.stringify(trimFormData));

    $.ajax({
      url: `${API_BASE_URL}/insertUser`,
      type: "POST",
      data: JSON.stringify(trimFormData),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      crossDomain: true,
      async: true,
      success: function (msg) {
        console.log(msg);
        onSigninClick();
        toast.success("Vous avez bien été inscrit !");
      },
      error: function (xhr, status, error) {
        console.log(error);
        toast.error("Une erreur s'est produite lors de l'inscription");
      },
    });
  };

  const renderSteps = () => {
    const steps = [];
    for (let i = 1; i <= TOTAL_STEPS; i++) {
      const isActive = i === currentStep;
      steps.push(
        <div className={`step ${isActive ? "active" : ""}`} key={i}>
          <div className="title">{STEP_TITLES[i - 1]}</div>
          <div className="circle">
            <div className="number">{i}</div>
          </div>
        </div>
      );
      if (i < TOTAL_STEPS) {
        steps.push(<div className="line" key={`line-${i}`}></div>);
      }
    }
    return steps;
  };

  return (
    <div>
      <div style={{ flexDirection: "column" }}>
        <h1>Inscription</h1>

        <div className={`form-container `}>
          <div className="progress-container">{renderSteps()}</div>
          <div className={`transition ${animationClass}`}>
            {currentStep === 1 && (
              <PersonalInfoForm
                formData={formData}
                setFormData={setFormData}
                nextStep={nextStep}
                onSigninClick={onSigninClick}
              />
            )}
            {currentStep === 2 && (
              <PreferenceForm
                formData={formData}
                setFormData={setFormData}
                prevStep={prevStep}
                nextStep={nextStep}
                onSigninClick={onSigninClick}
              />
            )}
            {currentStep === 3 && (
              <RecapForm
                formData={formData}
                prevStep={prevStep}
                handleSubmit={handleSubmit}
                onSigninClick={onSigninClick}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default SignupForm;
