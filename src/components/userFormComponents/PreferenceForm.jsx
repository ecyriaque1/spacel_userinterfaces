import React, { useState } from "react";
import { Form, Button, Row, Col, Alert } from "react-bootstrap";
function PreferenceForm({
  formData,
  setFormData,
  prevStep,
  nextStep,
  onSigninClick,
}) {
  const [usernameError, setUsernameError] = useState("");

  const [showAlert, setShowAlert] = useState(false);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));

    // Réinitialiser l'erreur du champ correspondant
    if (name === "username") {
      setUsernameError("");
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    // Valider les champs du formulaire
    const isValid = isUsernameValid(formData.username.trim());

    if (isValid && isCursorValid()) {
      nextStep();
      return;
    }
    return;
  };

  const isCursorValid = () => {
    if (
      formData.PreferredCollectiveLearningMode === -1 ||
      formData.PreferredIndividualLearningMode === -1 ||
      formData.PreferredFormatAudio === -1 ||
      formData.PreferredFormatText === -1 ||
      formData.PreferredFormatVideo === -1
    ) {
      setShowAlert(true);
      return false;
    } else {
      return true;
    }
  };

  const isUsernameValid = (username) => {
    const usernameRegex = /^[a-zA-Z0-9_-]{3,16}$/;
    if (!usernameRegex.test(username) || username.trim() === "") {
      setUsernameError("Veuillez saisir un nom d'utilisateur valide.");
      return false;
    } else {
      setUsernameError("");
      return true;
    }
  };

  return (
    <div>
      <h3>Préférences :</h3>
      <Form className="preference-form" onSubmit={handleSubmit}>
        <Row className="mb-3">
          <Form.Label column sm={2}>
            Nom d'utilisateur
          </Form.Label>
          <Col sm={10}>
            <Form.Group controlId="formUsername">
              <Form.Control
                type="text"
                name="username"
                placeholder="Nom d'utilisateur"
                value={formData.username}
                onChange={handleChange}
                required
                isInvalid={usernameError !== ""}
              />
              <Form.Control.Feedback type="invalid">
                {usernameError}
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
        </Row>

        <Form.Group controlId="formPreferredCollectiveLearningMode">
          <Form.Label>Mode d'apprentissage collectif préféré (%)</Form.Label>
          <Form.Control
            type="range"
            min="0"
            max="100"
            step="10"
            required
            name="PreferredCollectiveLearningMode"
            value={formData.PreferredCollectiveLearningMode}
            onChange={handleChange}
          />
          <Form.Text>{formData.PreferredCollectiveLearningMode}%</Form.Text>
        </Form.Group>
        <Form.Group controlId="formPreferredIndividualLearningMode">
          <Form.Label>Mode d'apprentissage individuel préféré (%)</Form.Label>
          <Form.Control
            type="range"
            min="0"
            max="100"
            step="10"
            name="PreferredIndividualLearningMode"
            value={formData.PreferredIndividualLearningMode}
            onChange={handleChange}
            required
          />
          <Form.Text>{formData.PreferredIndividualLearningMode}%</Form.Text>
        </Form.Group>

        <Form.Group controlId="formPreferredFormatAudio">
          <Form.Label>Format audio préféré (%)</Form.Label>
          <Form.Control
            type="range"
            min="0"
            max="100"
            step="10"
            required
            name="PreferredFormatAudio"
            value={formData.PreferredFormatAudio}
            onChange={handleChange}
          />
          <Form.Text>{formData.PreferredFormatAudio}%</Form.Text>
        </Form.Group>

        <Form.Group controlId="formPreferredFormatText">
          <Form.Label>Format texte préféré (%)</Form.Label>
          <Form.Control
            type="range"
            min="0"
            max="100"
            step="10"
            required
            name="PreferredFormatText"
            value={formData.PreferredFormatText}
            onChange={handleChange}
          />
          <Form.Text>{formData.PreferredFormatText}%</Form.Text>
        </Form.Group>

        <Form.Group controlId="formPreferredFormatVideo">
          <Form.Label>Format vidéo préféré (%)</Form.Label>
          <Form.Control
            type="range"
            min="0"
            max="100"
            step="10"
            required
            name="PreferredFormatVideo"
            value={formData.PreferredFormatVideo}
            onChange={handleChange}
          />
          <Form.Text>{formData.PreferredFormatVideo}%</Form.Text>
        </Form.Group>
        {showAlert && (
          <Alert variant="danger" onClose={() => setShowAlert(false)}>
            <p>Veuillez interagir avec tous les curseurs avant de continuer.</p>
          </Alert>
        )}
        <hr />
        <Row>
          <Col
            sm={{ span: 10, offset: 2 }}
            className="d-flex align-items-center"
          >
            <Button variant="secondary" onClick={prevStep} className="mr-2">
              Précédent
            </Button>
            <Button variant="primary" type="submit" className="mr-2">
              Suivant
            </Button>
            <span>
              Vous possédez déjà un compte?
              <button
                style={{
                  background: "none",
                  border: "none",
                  padding: 0,
                  textDecoration: "underline",
                  cursor: "pointer",
                  color: "blue",
                }}
                onClick={onSigninClick}
              >
                Créez un compte
              </button>
            </span>
          </Col>
        </Row>
      </Form>
    </div>
  );
}

export default PreferenceForm;
