import { useContext, useState } from "react";
import { Form, Button, Row, Col, Alert } from "react-bootstrap";
import $ from "jquery";
import "./css/SigninForm.css";
import Auth from "../../contexts/Auth";
import { useNavigate } from "react-router-dom";
import { API_BASE_URL } from "../../config/config.js";
import ToastContext from "../../contexts/ToastContext";
function SigninForm({ onSignupClick }) {
  const [EmailError, setEmailError] = useState("");
  const [PasswordError, setPasswordError] = useState("");
  const { setIsAuthenticated } = useContext(Auth);
  const toast = useContext(ToastContext);
  const navigate = useNavigate();

  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };
  const handleSubmit = (event) => {
    event.preventDefault();

    // Reset error messages
    setEmailError("");
    setPasswordError("");

    // Validate form fields
    if (formData.email === "") {
      setEmailError("Veuillez saisir votre adresse e-mail.");
      return;
    }

    if (formData.password === "") {
      setPasswordError("Veuillez saisir votre mot de passe.");
      return;
    }

    // Prepare data for POST request
    const userData = {
      email: formData.email,
      password: formData.password,
    };
    toast.success("Connexion réussie.");
    sessionStorage.setItem("isAuthenticated", "true");
    const user = {
      idUser: 1,
      name: "John Doe",
    };
    sessionStorage.setItem("userData", JSON.stringify(user));
    setIsAuthenticated(true);
    navigate("/");
    // Send POST request using jQuery
    // $.ajax({
    //   url: `${API_BASE_URL}/userLogin`,
    //   type: "POST",
    //   data: JSON.stringify(userData),
    //   contentType: "application/json; charset=utf-8",
    //   dataType: "json",
    //   crossDomain: true,
    //   async: true,
    //   success: function (msg) {
    //     console.log(msg);

    //     if (msg.length > 0) {
    //       // Stocker les données dans le sessionStorage
    //       sessionStorage.setItem("userData", JSON.stringify(msg[0]));
    //       toast.success("Connexion réussie.");
    //       sessionStorage.setItem("isAuthenticated", "true");
    //       setIsAuthenticated(true);
    //       navigate("/training");
    //       console.log(msg);
    //       // Autres actions à effectuer après avoir stocké les données
    //     } else {
    //       // Afficher un toast pour indiquer que le mot de passe ou l'email est incorrect
    //       toast.error("Erreur : mot de passe ou email incorrect.");
    //     }
    //   },
    //   error: function (xhr, textStatus, errorThrown) {
    //     if (textStatus === "timeout") {
    //       alert("Erreur de chargement de la ressource : Connexion expirée.");
    //     }
    //   },
    // });
  };

  return (
    <div>
      <div style={{ flexDirection: "column" }}>
        <h1>Connexion</h1>
        <div className={`form-container `}>
          <div className={`transition`}>
            <div>
              <Form
                style={{ height: "100%" }}
                className="signin-form d-flex flex-column justify-content-center"
                onSubmit={handleSubmit}
              >
                <Row className="mb-3 flex-grow-1 d-flex flex-column">
                  <Form.Label
                    column
                    sm={3}
                    className="d-flex align-items-center"
                  >
                    Email
                  </Form.Label>
                  <Col sm={9}>
                    <Form.Group controlId="formEmail" className="flex-grow-1">
                      <Form.Control
                        placeholder="Email"
                        type="email"
                        name="email"
                        value={formData.email}
                        onChange={handleChange}
                        required
                        style={{ height: "100%" }} // Added height style
                      />
                      {EmailError && (
                        <Alert variant="danger" className="alert-custom">
                          {EmailError}
                        </Alert>
                      )}
                    </Form.Group>
                  </Col>
                </Row>
                <Row className="mb-3 flex-grow-1 d-flex flex-column">
                  <Form.Label
                    column
                    sm={3}
                    className="d-flex align-items-center"
                  >
                    Mot de passe
                  </Form.Label>
                  <Col sm={9}>
                    <Form.Group
                      controlId="formPassword"
                      className="flex-grow-1"
                    >
                      <Form.Control
                        placeholder="Mot de passe"
                        type="password"
                        name="password"
                        value={formData.password}
                        onChange={handleChange}
                        required
                        style={{ height: "100%" }} // Added height style
                      />
                      {PasswordError && (
                        <Alert variant="danger" className="alert-custom">
                          {PasswordError}
                        </Alert>
                      )}
                    </Form.Group>
                  </Col>
                </Row>
                <Row className="mb-3 flex-grow-1">
                  <Col
                    sm={12}
                    md={{ span: 8, offset: 2 }}
                    className="d-flex justify-content-center align-items-center"
                  >
                    <Button
                      style={{ width: "50%" }}
                      variant="primary"
                      type="submit"
                    >
                      Connexion
                    </Button>
                    <span className="ml-2">
                      Vous n'avez pas de compte?
                      <button
                        style={{
                          background: "none",
                          border: "none",
                          padding: 0,
                          textDecoration: "underline",
                          cursor: "pointer",
                          color: "blue",
                        }}
                        onClick={onSignupClick}
                      >
                        Créez un compte
                      </button>
                    </span>
                  </Col>
                </Row>
              </Form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SigninForm;
