import React, { useContext, useState } from "react";
import { Navbar, Container, Nav, Button } from "react-bootstrap";
import Auth from "../../contexts/Auth";
import ToastContext from "../../contexts/ToastContext";
import { Link } from "react-router-dom";

function AuthNavBar() {
  const { isAuthenticated, setIsAuthenticated } = useContext(Auth);
  const toast = useContext(ToastContext);
  const [expanded, setExpanded] = useState(false); // Ajout de l'état pour gérer l'ouverture/fermeture de la navbar sur mobile
  var username = "";
  if (isAuthenticated) {
    var userData = JSON.parse(sessionStorage.getItem("userData"));
    username = userData.name + userData.idUser;
  }

  const handleLogout = () => {
    sessionStorage.removeItem("isAuthenticated");
    setIsAuthenticated(false);
    toast.info("Vous avez été déconnecté !");
  };

  return (
    <Navbar
      bg="dark"
      variant="dark"
      expand="lg"
      className="navbar-custom"
      expanded={expanded} // Ajout de la prop 'expanded' pour gérer l'ouverture/fermeture de la navbar sur mobile
    >
      <Container fluid>
        <Navbar.Brand as={Link} to="/">
          Spac-El
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="navbarScroll"
          onClick={() => setExpanded(!expanded)} // Inversion de l'état 'expanded' lorsqu'on clique sur le bouton de réduction
        />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: "90px" }}
            navbarScroll
          >
            <Nav.Link as={Link} to="/">
              Portfolio
            </Nav.Link>
          </Nav>
          <Nav>
            {!isAuthenticated ? (
              <Nav.Link as={Link} to="/AuthentificationForm">
                Connexion/Inscription
              </Nav.Link>
            ) : (
              <>
                <Nav.Link as={Link} to="/training">
                  Trouvez votre formation
                </Nav.Link>
                <Button variant="danger" onClick={handleLogout}>
                  Déconnexion
                </Button>
                <span className="text-light ml-2">{username}</span>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default AuthNavBar;
