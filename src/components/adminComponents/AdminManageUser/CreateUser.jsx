import React, { useState } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";

const CreateUser = () => {
  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
    email: "",
    gender: "",
    password: "",
    confirmPassword: "",
    role: "",
    username: "",
  });

  const [errors, setErrors] = useState({});

  const handleChange = (e) => {
    setUser({ ...user, [e.target.name]: e.target.value });
  };

  const validateForm = () => {
    let tempErrors = {};

    tempErrors.firstName = user.firstName ? "" : "Le prénom est requis.";
    tempErrors.lastName = user.lastName ? "" : "Le nom est requis.";
    tempErrors.email = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/.test(
      user.email
    )
      ? ""
      : "Email n'est pas valide.";
    tempErrors.password =
      /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z])(?=.*[!@#$%^&*]).{8,}$/.test(
        user.password
      )
        ? ""
        : "Le mot de passe doit contenir au moins 8 caractères, 1 chiffre, 1 majuscule et 1 caractère spécial.";
    tempErrors.confirmPassword =
      user.password === user.confirmPassword
        ? ""
        : "Les mots de passe ne correspondent pas.";

    tempErrors.gender = user.gender ? "" : "Veuillez sélectionner le sexe.";
    tempErrors.role = user.role ? "" : "Veuillez sélectionner le rôle.";
    tempErrors.username = user.username
      ? ""
      : "Le nom d'utilisateur est requis.";

    setErrors({ ...tempErrors });

    return Object.values(tempErrors).every((x) => x === "");
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const isValid = validateForm();
    if (isValid) {
      const userData = {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        gender: user.gender,
        password: user.password,
        role: user.role,
        username: user.username,
      };
      alert(JSON.stringify(userData));
      setUser({
        firstName: "",
        lastName: "",
        email: "",
        gender: "",
        password: "",
        confirmPassword: "",
        role: "",
        username: "",
      });
    }
  };

  return (
    <Container>
      <h2>Créer un utilisateur</h2>
      <Form onSubmit={handleSubmit}>
        <Form.Group as={Row} controlId="formFirstName">
          <Form.Label column sm="2">
            Prénom
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="text"
              name="firstName"
              placeholder="Prénom"
              onChange={handleChange}
              isInvalid={!!errors.firstName}
              value={user.firstName}
            />
            <Form.Control.Feedback type="invalid">
              {errors.firstName}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formLastName">
          <Form.Label column sm="2">
            Nom
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="text"
              name="lastName"
              placeholder="Nom"
              onChange={handleChange}
              isInvalid={!!errors.lastName}
              value={user.lastName}
            />
            <Form.Control.Feedback type="invalid">
              {errors.lastName}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formUsername">
          <Form.Label column sm="2">
            Nom d'utilisateur
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="text"
              name="username"
              placeholder="Nom d'utilisateur"
              onChange={handleChange}
              isInvalid={!!errors.username}
              value={user.username}
            />
            <Form.Control.Feedback type="invalid">
              {errors.username}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formEmail">
          <Form.Label column sm="2">
            Email
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="email"
              name="email"
              placeholder="Email"
              onChange={handleChange}
              isInvalid={!!errors.email}
              value={user.email}
            />
            <Form.Control.Feedback type="invalid">
              {errors.email}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formGender">
          <Form.Label column sm="2">
            Genre
          </Form.Label>
          <Col sm="10">
            <Form.Control
              as="select"
              name="gender"
              onChange={handleChange}
              isInvalid={!!errors.gender}
              value={user.gender}
            >
              <option value="">Sélectionnez le sexe</option>
              <option value="male">Homme</option>
              <option value="female">Femme</option>
              <option value="other">Autre</option>
            </Form.Control>
            <Form.Control.Feedback type="invalid">
              {errors.gender}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formPassword">
          <Form.Label column sm="2">
            Mot de passe
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="password"
              name="password"
              placeholder="Mot de passe"
              onChange={handleChange}
              isInvalid={!!errors.password}
              value={user.password}
            />
            <Form.Control.Feedback type="invalid">
              {errors.password}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formConfirmPassword">
          <Form.Label column sm="2">
            Confirmez le mot de passe
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="password"
              name="confirmPassword"
              placeholder="Confirmez le mot de passe"
              onChange={handleChange}
              isInvalid={!!errors.confirmPassword}
              value={user.confirmPassword}
            />
            <Form.Control.Feedback type="invalid">
              {errors.confirmPassword}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formRole">
          <Form.Label column sm="2">
            Rôle
          </Form.Label>
          <Col sm="10">
            <Form.Control
              as="select"
              name="role"
              onChange={handleChange}
              isInvalid={!!errors.role}
              value={user.role}
            >
              <option value="">Sélectionnez le rôle</option>
              <option value="admin">Admin</option>
              <option value="user">Utilisateur</option>
            </Form.Control>
            <Form.Control.Feedback type="invalid">
              {errors.role}
            </Form.Control.Feedback>
          </Col>
        </Form.Group>

        <Button variant="primary" type="submit">
          Créer un utilisateur
        </Button>
      </Form>
    </Container>
  );
};

export default CreateUser;
