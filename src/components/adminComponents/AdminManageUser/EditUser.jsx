import React, { useState } from "react";
import { Container, Form, Row, Col, Button } from "react-bootstrap";

const EditUser = ({ user, showDeleteConfirmation }) => {
  const [formData, setFormData] = useState({
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
    gender: user.gender,
    username: user.username,
    role: user.role,
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData((prevFormData) => ({
      ...prevFormData,
      [name]: value,
    }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    // Perform actions with the updated data (e.g., send it to the server)
    console.log(formData);
  };

  return (
    <Container>
      <h2>Modifier l'utilisateur</h2>
      <Form onSubmit={handleSubmit}>
        <Form.Group as={Row} controlId="formFirstName">
          <Form.Label column sm="2">
            Prénom
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="text"
              name="firstName"
              placeholder="Prénom"
              onChange={handleChange}
              value={formData.firstName}
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formLastName">
          <Form.Label column sm="2">
            Nom
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="text"
              name="lastName"
              placeholder="Nom"
              onChange={handleChange}
              value={formData.lastName}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formUsername">
          <Form.Label column sm="2">
            Nom d'utilisateur
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="text"
              name="username"
              placeholder="Nom d'utilisateur"
              onChange={handleChange}
              value={formData.username}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formEmail">
          <Form.Label column sm="2">
            Email
          </Form.Label>
          <Col sm="10">
            <Form.Control
              type="email"
              name="email"
              placeholder="Email"
              onChange={handleChange}
              value={formData.email}
            />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formGender">
          <Form.Label column sm="2">
            Genre
          </Form.Label>
          <Col sm="10">
            <Form.Control
              as="select"
              name="gender"
              onChange={handleChange}
              value={formData.gender}
            >
              <option value="">Sélectionnez le sexe</option>
              <option value="male">Homme</option>
              <option value="female">Femme</option>
              <option value="other">Autre</option>
            </Form.Control>
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formRole">
          <Form.Label column sm="2">
            Rôle
          </Form.Label>
          <Col sm="10">
            <Form.Control
              as="select"
              name="role"
              onChange={handleChange}
              value={formData.role}
            >
              <option value="">Sélectionnez le rôle</option>
              <option value="admin">Admin</option>
              <option value="user">Utilisateur</option>
            </Form.Control>
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="formButtons" className="d-flex">
          <Col sm="2"></Col>
          <Col sm="10" className="d-flex justify-content-between">
            {" "}
            <Button
              variant="danger"
              style={{ marginTop: "9px", marginRight: "10px" }}
              className="btn-block"
              onClick={() => showDeleteConfirmation(user)}
            >
              Supprimer
            </Button>
            <Button variant="primary" type="submit" className="btn-block">
              Enregistrer
            </Button>
          </Col>
        </Form.Group>
      </Form>
    </Container>
  );
};

export default EditUser;
