import { React, useState } from "react";
import { Table, Button, Form } from "react-bootstrap";
import "./ManageUser.css";
import { useMediaQuery } from "react-responsive";

const users = [
  {
    id: 1,
    firstName: "John",
    lastName: "Doe",
    email: "johndoe@example.com",
    gender: "male",
    username: "johndoe",
    role: "Admin",
  },
  {
    id: 2,
    firstName: "Jane",
    lastName: "Smith",
    email: "janesmith@example.com",
    gender: "female",
    username: "janesmith",
    role: "User",
  },
  {
    id: 3,
    firstName: "Bob",
    lastName: "Johnson",
    email: "bobjohnson@example.com",
    gender: "male",
    username: "bobjohnson",
    role: "User",
  },
  {
    id: 4,
    firstName: "Alice",
    lastName: "Williams",
    email: "alicewilliams@example.com",
    gender: "female",
    username: "alicewilliams",
    role: "User",
  },
  {
    id: 5,
    firstName: "Michael",
    lastName: "Brown",
    email: "michaelbrown@example.com",
    gender: "male",
    username: "michaelbrown",
    role: "User",
  },
  {
    id: 6,
    firstName: "Emily",
    lastName: "Jones",
    email: "emilyjones@example.com",
    gender: "female",
    username: "emilyjones",
    role: "User",
  },
  {
    id: 7,
    firstName: "David",
    lastName: "Taylor",
    email: "davidtaylor@example.com",
    gender: "male",
    username: "davidtaylor",
    role: "User",
  },
  {
    id: 8,
    firstName: "Sophia",
    lastName: "Wilson",
    email: "sophiawilson@example.com",
    gender: "female",
    username: "sophiawilson",
    role: "User",
  },
  {
    id: 9,
    firstName: "James",
    lastName: "Anderson",
    email: "jamesanderson@gmail.com",
    gender: "male",
    username: "jamesanderson",
    role: "User",
  },
  {
    id: 10,
    firstName: "Olivia",
    lastName: "Thomas",
    email: "oliviathomas@example.com",
    gender: "female",
    username: "oliviathomas",
    role: "User",
  },
];

function UserManagement({ handleEditUser, showDeleteConfirmation }) {
  const [sortBy, setSortBy] = useState("");
  const [searchTerm, setSearchTerm] = useState("");
  const handleSortChange = (e) => {
    setSortBy(e.target.value);
  };

  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-device-width: 1224px)",
  });

  const isMobile = useMediaQuery({ query: "(max-width: 1224px)" });

  const handleSearchChange = (e) => {
    setSearchTerm(e.target.value);
  };

  const sortedUsers = [...users].sort((a, b) => {
    if (sortBy === "name") {
      return a.firstName.localeCompare(b.firstName);
    } else if (sortBy === "email") {
      return a.email.localeCompare(b.email);
    } else {
      return 0;
    }
  });

  // Recherche dans les utilisateurs
  const filteredUsers = sortedUsers.filter(
    (user) =>
      `${user.firstName} ${user.lastName}`
        .toLowerCase()
        .includes(searchTerm.toLowerCase()) ||
      user.email.toLowerCase().includes(searchTerm.toLowerCase())
  );
  return (
    <div className="user-management">
      <h2 className="user-management-title">Gestion des utilisateurs</h2>

      <div className="user-management-controls">
        <Form>
          <div className="row align-items-center">
            <div className="col-auto">
              <h2 style={{ fontSize: "1.5rem" }}>Liste des utilisateurs</h2>
            </div>
            <div className="col-auto">
              <Form.Label style={{ fontSize: "1rem" }}>Trier par :</Form.Label>
            </div>
            <div className="col-auto">
              <Form.Control
                as="select"
                value={sortBy}
                onChange={handleSortChange}
                style={{ fontSize: "1rem" }}
              >
                <option value="">Choisir...</option>
                <option value="name">Nom</option>
                <option value="email">Email</option>
              </Form.Control>
            </div>
            <div className="col-auto">
              <Form.Label style={{ fontSize: "1rem" }}>Rechercher :</Form.Label>
            </div>
            <div className="col-auto">
              <Form.Control
                type="text"
                placeholder="Nom ou email"
                value={searchTerm}
                onChange={handleSearchChange}
                style={{ fontSize: "1rem" }}
              />
            </div>
          </div>
        </Form>
      </div>
      {isDesktopOrLaptop && (
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>ID</th>
              <th>Nom</th>
              <th>Email</th>
              <th>Genre</th>
              <th>Nom d'utilisateur</th>
              <th>Rôle</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {filteredUsers.map((user) => (
              <tr key={user.id}>
                <td>{user.id}</td>
                <td>{`${user.firstName} ${user.lastName}`}</td>
                <td>{user.email}</td>
                <td>{user.gender}</td>
                <td>{user.username}</td>
                <td>{user.role}</td>
                <td>
                  <Button
                    variant="danger"
                    style={{ backgroundColor: "#FF6464" }}
                    onClick={() => showDeleteConfirmation(user)}
                  >
                    Supprimer
                  </Button>{" "}
                  <Button
                    variant="primary"
                    style={{ backgroundColor: "#1EAAF1" }}
                    onClick={() => handleEditUser(user)}
                  >
                    Modifier
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      )}
      {isMobile && (
        <div className="user-cards">
          {filteredUsers.map((user) => (
            <div key={user.id} className="user-card">
              <h3>{`${user.firstName} ${user.lastName}`}</h3>
              <p>ID: {user.id}</p>
              <p>Email: {user.email}</p>
              <p>Genre: {user.gender}</p>
              <p>Nom d'utilisateur: {user.username}</p>
              <p>Rôle: {user.role}</p>
              <Button
                variant="danger"
                style={{ backgroundColor: "#FF6464" }}
                onClick={() => showDeleteConfirmation(user)}
              >
                Supprimer
              </Button>{" "}
              <Button
                variant="primary"
                style={{ backgroundColor: "#1EAAF1" }}
                onClick={() => handleEditUser(user)}
              >
                Modifier
              </Button>
            </div>
          ))}
        </div>
      )}
    </div>
  );
}

export default UserManagement;
