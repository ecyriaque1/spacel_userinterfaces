import React, { useContext, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import Auth from "../contexts/Auth";
import ToastContext from "../contexts/ToastContext";
const ProtectedRoute = ({ redirectLink, requireAuth = true }) => {
  const { isAuthenticated } = useContext(Auth);
  const navigate = useNavigate();
  const toast = useContext(ToastContext);
  useEffect(() => {
    if (requireAuth && !isAuthenticated) {
      navigate(redirectLink);
      toast.error("Vous devez être connecté pour accéder à cette page");
    } else if (!requireAuth && isAuthenticated) {
      navigate(redirectLink);
      toast.error("Vous êtes déjà connecté");
    }
  }, [requireAuth, isAuthenticated, navigate, redirectLink, toast]);
  return <></>;
};

export default ProtectedRoute;
