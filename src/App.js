import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import AuthentificationForm from "./pages/AuthentificationForm";
import Home from "./pages/Home";
import Admin from "./pages/AdminPage";
import { useState } from "react";

import Auth from "./contexts/Auth";
import AuthNavBar from "./components/navbar/AuthNavBar";
import { toast, ToastContainer } from "react-toastify";
import ToastContext from "./contexts/ToastContext";
import TrainingPage from "./pages/TrainingPage";
import AfficherFormation from "./pages/AfficherFormation";

function App() {
  const [isAuthenticated, setIsAuthenticated] = useState(
    sessionStorage.getItem("isAuthenticated") === "true"
  );

  return (
    <Auth.Provider value={{ isAuthenticated, setIsAuthenticated }}>
      <ToastContext.Provider value={toast}>
        <Router>
          <div>
            <ToastContainer />
            <AuthNavBar />
            <Routes>
              <Route path="/" element={<Home />} />
              <Route
                path="/AuthentificationForm"
                element={<AuthentificationForm />}
              />
              <Route path="/admin" element={<Admin />} />
              <Route
                path="/afficherFormation"
                element={<AfficherFormation />}
              />
              <Route path="/training" element={<TrainingPage />} />
            </Routes>
          </div>
        </Router>
      </ToastContext.Provider>
    </Auth.Provider>
  );
}
export default App;
